const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

// Recursive function to calculate factorial
function factorial(n) {
    if (n === 0) {
        return 1;
    } else {
        return n * factorial(n - 1);
    }
}


rl.question('Enter a number to calculate its factorial: ', (input) => {
    const number = parseInt(input);

    if (isNaN(number)) {
        console.log('Invalid input. Please enter a valid number.');
    } else if (number < 0) {
        console.log('Factorial is not defined for negative numbers.');
    } else {
        const result = factorial(number);
        console.log(`Factorial of ${number} is: ${result}`);
    }

    rl.close();
});
